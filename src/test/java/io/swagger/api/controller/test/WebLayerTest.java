package io.swagger.api.controller.test;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@SpringBootTest
@AutoConfigureMockMvc
public class WebLayerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnCreatePetMessage() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders
						.post("/pet/")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"category\": {\"id\": 0,\"name\": \"string\"},"
								+"\"id\": 0,\"name\": \"rocko motrocko\",\"photoUrls\": [\"string\"],"
								+"\"status\": \"available\",\"tags\": [ {\"id\": 0,\"name\": \"string\"}]}")
							)
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message").value("pet created"));
	}

	@Test
	public void shouldReturnUnauthorizedMessage() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders
					.delete("/pet/{id}", "11")
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized())
				.andExpect(status().reason(containsString("Unauthorized")));
	}
}