package io.swagger.api.exceptions;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")
public class ApiException extends Exception{
    private static final long serialVersionUID = 7031220368095579662L;
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }

    public int getCode(){
        return this.code;
    }
}
