package io.swagger.api.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.swagger.model.ModelApiResponse;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler(value = { ApiException.class })
    protected ResponseEntity<Object> handleApieException(Exception ex, WebRequest request) {
        ModelApiResponse error = new ModelApiResponse(500,"ERROR" ,ex.getMessage());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, error,  new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = {  NotFoundException.class })
    protected ResponseEntity<Object> handleNotFoundException(Exception ex, WebRequest request) {
        ModelApiResponse error = new ModelApiResponse(404,"NOT FOUND" ,ex.getMessage());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, error,  new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}