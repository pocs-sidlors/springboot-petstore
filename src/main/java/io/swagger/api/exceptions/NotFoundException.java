package io.swagger.api.exceptions;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")

public class NotFoundException extends ApiException {
    private static final long serialVersionUID = 4295369162500807573L;
    private int code;
    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }

    public int getCode(){
        return this.code;
    }
}
