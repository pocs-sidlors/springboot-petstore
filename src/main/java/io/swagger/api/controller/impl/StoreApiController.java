package io.swagger.api.controller.impl;

import java.util.List;
import java.util.Optional;
import io.swagger.model.Order;
import io.swagger.annotations.*;
import io.swagger.api.controller.StoreApi;
import io.swagger.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.constraints.*;
import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")
@Controller
public class StoreApiController implements StoreApi {

    @Autowired
    OrderService service;


    public ResponseEntity<Void> deleteOrder( @Min(1)@ApiParam(value = "ID of the order that needs to be deleted",required=true ) @PathVariable("orderId") Long orderId) {
        // do some magic!
        service.deleteOrder(orderId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<List<Order>> getInventory() {
        // do some magic!
        List<Order> inventory = service.getInventory();
        return new ResponseEntity<List<Order>>(inventory,HttpStatus.OK);
    }

    public ResponseEntity<Order> getOrderById( @Min(1) @Max(10)@ApiParam(value = "ID of pet that needs to be fetched",required=true ) @PathVariable("orderId") Long orderId) {
        // do some magic!
        Optional<Order> orderById = service.getOrderById(orderId);
        if(orderById.isPresent()){
            return new ResponseEntity<Order>(orderById.get(),HttpStatus.OK);
        }
        return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Order> placeOrder(@ApiParam(value = "order placed for purchasing the pet" ,required=true )  @Valid @RequestBody Order body) {
        service.placeOrder(body);
        return new ResponseEntity<Order>(HttpStatus.OK);
    }

}
