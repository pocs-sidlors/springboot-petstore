package io.swagger.api.controller.impl;

import io.swagger.model.ModelApiResponse;
import io.swagger.model.Pet;
import io.swagger.api.controller.PetApi;
import io.swagger.api.exceptions.ApiException;
import io.swagger.api.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Optional;
import javax.validation.constraints.*;
import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")
@Controller
public class PetApiController implements PetApi {

    @Autowired
    PetService service;

    @RequestMapping(value = "/pet",
    produces = { "application/json" }, 
    consumes = { "application/json"  },
    method = RequestMethod.POST)
    public ResponseEntity<ModelApiResponse> addPet(@Valid @RequestBody Pet body) {
        Optional<Pet> petById = service.getPetById(body.getId());
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (!petById.isPresent()) {
            modelApiResponse.setCode(201);
            modelApiResponse.setMessage("pet created");
            modelApiResponse.setType("success");
            service.addPet(body);
            return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.CREATED);
        }
        modelApiResponse.setCode(405);
        modelApiResponse.setMessage("Invalid input");
        modelApiResponse.setType("duplicated id");
        return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @RequestMapping(value = "/pet/{petId}",
    produces = { "application/json" }, 
    method = RequestMethod.DELETE)
    public ResponseEntity<ModelApiResponse> deletePet( @PathVariable("petId") Long petId,
            @RequestHeader(value = "Authorization", required = true) String Authorization) {
        Optional<Pet> petById = service.getPetById(petId);
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (petById.isPresent()) {
            service.deletePet(petId);
            modelApiResponse.setCode(200);
            modelApiResponse.setMessage("pet deteled");
            modelApiResponse.setType("success");
            return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.OK);
        }
        modelApiResponse.setCode(400);
        modelApiResponse.setMessage("Invalid ID supplied");
        modelApiResponse.setType("error");
        return new ResponseEntity<ModelApiResponse>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/pet/findByStatus",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<List<Pet>> findPetsByStatus(
            @NotNull @RequestParam(value = "status", required = true) List<String> status) {
        List<Pet> findPetsByStatus = service.findPetsByStatus(status);
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (!findPetsByStatus.isEmpty()) {
            modelApiResponse.setCode(200);
            modelApiResponse.setMessage("successful operation");
            modelApiResponse.setType("success");
            return new ResponseEntity<List<Pet>>(findPetsByStatus, HttpStatus.OK);
        }
        modelApiResponse.setCode(404);
        modelApiResponse.setMessage("Invalid status value");
        modelApiResponse.setType("empty retrive");
        return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/pet/findByTags",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<List<Pet>> findPetsByTags(
            @NotNull @RequestParam(value = "tags", required = true) List<String> tags) {
        List<Pet> findPetsTags = service.findPetsByTags(tags);
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (!findPetsTags.isEmpty()) {
            modelApiResponse.setCode(200);
            modelApiResponse.setMessage("successful operation");
            modelApiResponse.setType("success");
            return new ResponseEntity<List<Pet>>(findPetsTags, HttpStatus.OK);
        }
        modelApiResponse.setCode(404);
        modelApiResponse.setMessage("Invalid status value");
        modelApiResponse.setType("empty retrive");
        return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/pet/{petId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<Pet> getPetById(@PathVariable("petId") Long petId) {
        Optional<Pet> petById = service.getPetById(petId);

        if (petById.isPresent()) {
            return new ResponseEntity<Pet>(petById.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/pet",
        produces = { "application/json" }, 
        consumes = { "application/json",  },
        method = RequestMethod.PUT)
        public ResponseEntity<ModelApiResponse> updatePet(@Valid @RequestBody Pet body) {
        Optional<Pet> petById = service.getPetById(body.getId());
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (petById.isPresent()) {
            service.updatePet(petById.get());
            modelApiResponse.setCode(200);
            modelApiResponse.setMessage("pet uploaded");
            modelApiResponse.setType("success");
            return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.OK);
        }

        modelApiResponse.setCode(404);
        modelApiResponse.setMessage("Pet not found");
        modelApiResponse.setType("error");
        return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/pet/{petId}",
        produces = { "application/json" }, 
        consumes = { "application/x-www-form-urlencoded" },
        method = RequestMethod.POST)
      public ResponseEntity<ModelApiResponse> updatePetWithForm(
            @PathVariable("petId") Long petId,
            @RequestPart(value = "name", required = false) String name,
            @RequestPart(value = "status", required = false) String status){
            Optional<Pet> petById = service.getPetById(petId);
            ModelApiResponse modelApiResponse = new ModelApiResponse();
    
            if (petById.isPresent()) {
                service.updatePetWithForm(petId, name, status);
                modelApiResponse.setCode(200);
                modelApiResponse.setMessage("pet uploaded");
                modelApiResponse.setType("success");
                return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.OK);
            }
    
            modelApiResponse.setCode(404);
            modelApiResponse.setMessage("Pet not found");
            modelApiResponse.setType("error");
            return new ResponseEntity<ModelApiResponse>(modelApiResponse, HttpStatus.NOT_FOUND);
        }

    @RequestMapping(value = "/pet/{petId}/uploadImage",
    produces = { "application/json" }, 
    consumes = { "multipart/form-data" },
    method = RequestMethod.POST)
    public ResponseEntity<ModelApiResponse> uploadFile(
            @PathVariable("petId") Long petId,
            @RequestPart(value = "additionalMetadata", required = false) String additionalMetadata,
            @RequestPart("file") MultipartFile file) {
        Optional<Pet> petById = service.getPetById(petId);
        ModelApiResponse modelApiResponse = new ModelApiResponse();

        if (petById.isPresent()) {
            try {
                service.uploadFile(petId, additionalMetadata, file);
                modelApiResponse.setCode(200);
                modelApiResponse.setMessage("pet file uploaded");
                modelApiResponse.setType("success");
                return new ResponseEntity<ModelApiResponse>(modelApiResponse,HttpStatus.OK);
            } catch (ApiException e) {
                modelApiResponse.setCode(e.getCode());
                modelApiResponse.setMessage(e.getMessage());
                modelApiResponse.setType("error");
                return new ResponseEntity<ModelApiResponse>(modelApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            modelApiResponse.setCode(404);
            modelApiResponse.setMessage("Pet not found");
            modelApiResponse.setType("error");
            return new ResponseEntity<ModelApiResponse>(modelApiResponse,HttpStatus.NOT_FOUND);
        }
    }


}
