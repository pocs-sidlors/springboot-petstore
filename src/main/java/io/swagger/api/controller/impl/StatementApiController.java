package io.swagger.api.controller.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import io.swagger.api.controller.Statement;
import io.swagger.api.exceptions.ApiException;
import io.swagger.api.exceptions.NotFoundException;
import io.swagger.model.Order;
import io.swagger.util.Util;

@Controller
public class StatementApiController implements Statement {

    @GetMapping(value = "/statement/order/{orderId}/recipt", produces = { "*/*" })
    public ResponseEntity<InputStreamResource> getRecipt(@Min(1) @Max(10) @PathVariable("orderId") Long orderId)
            throws ApiException {

        byte[] decoded=null;
        String fileName="notFound.txt";
        if(orderId==1){
            fileName="recipt.txt";
        }
        try {
            decoded = Base64.getDecoder().decode(Util.getFileContentBase64(fileName));
        } catch (IOException e) {
            throw new ApiException(501, "pdf creation file not found");
        }
        InputStream pdfFile = new ByteArrayInputStream(decoded);

        return ResponseEntity
                .ok()
                .contentType(
                        MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(pdfFile));
    }

    @GetMapping(value = "/statement/order/{orderId}",produces = {  "application/json" })
    public ResponseEntity<Order> getOrderById(@Min(1) @Max(10)@PathVariable("orderId") Long orderId) throws NotFoundException {
        throw new NotFoundException(403,"resource not found");
    }
    
}