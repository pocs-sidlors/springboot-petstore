package io.swagger.api.controller;

import io.swagger.annotations.*;
import io.swagger.api.exceptions.ApiException;
import io.swagger.api.exceptions.NotFoundException;
import io.swagger.model.Order;
import io.swagger.annotations.*;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import javax.validation.constraints.*;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")
@Api(value = "statement", description = "the statement API")
public interface Statement {

    
    @ApiOperation(value = "Returns pet recipt order"
                , notes = "Returns a pdf recipt"
                , response = Byte.class, responseContainer = "Map", authorizations = {
        @Authorization(value = "api_key")
    })
    @ApiResponses(value = { 
        @ApiResponse(code = 200
                    , message = "successful operation"
                    , response = Void.class
                    , responseContainer = "Map") })
    ResponseEntity<InputStreamResource> getRecipt(@Min(1) @Max(10)@ApiParam(value = "ID of pet that needs to be fetched",required=true )  Long orderId) throws ApiException ;



    @ApiOperation(value = "Find purchase order by ID"
                , notes = "For valid response try integer IDs with value >= 1 and <= 10.         Other values will generated exceptions"
                , response = Order.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Order.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied", response = Void.class),
        @ApiResponse(code = 404, message = "Order not found", response = Void.class) })
    ResponseEntity<Order> getOrderById( @Min(1) @Max(10)@ApiParam(value = "ID of pet that needs to be fetched",required=true )  Long orderId)throws NotFoundException;

    
}
