package io.swagger.api.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T15:28:50.241Z")
@WebFilter(filterName = "apiOriginFilter"
, urlPatterns = {"/pet/*","/user/*","/store/*"}
)
public class ApiOriginFilter implements javax.servlet.Filter {

    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader("Access-Control-Allow-Headers", "Content-Type");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        System.out.println("destroy filter here");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init filter here");
    }
}
