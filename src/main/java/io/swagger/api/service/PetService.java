package io.swagger.api.service;

import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import io.swagger.api.exceptions.ApiException;
import io.swagger.model.Pet;

public interface PetService {

	void addPet(Pet body);

	void deletePet(Long petId);

	List<Pet> findPetsByStatus(List<String> status);

	List<Pet> findPetsByTags(List<String> tags);

	void uploadFile(Long petId, String additionalMetadata, MultipartFile file) throws ApiException;

	void updatePetWithForm(Long petId, String name, String status);

	Optional<Pet> getPetById(Long petId);

	void updatePet(Pet body);

    
}