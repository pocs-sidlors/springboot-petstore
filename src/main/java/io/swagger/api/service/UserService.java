package io.swagger.api.service;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import io.swagger.model.User;

public interface UserService {

	void createUser(@Valid User body);

	void createUsersWithArrayInput(@Valid List<User> body);

	void deleteUser(String username);

	Optional<User> getUserByName(String username);

	void loginUser(@NotNull String username, @NotNull String password);

	void logoutUser();

	void updateUser(String username, @Valid User body);
    
}