package io.swagger.api.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import io.swagger.model.Order;

@Service
public interface OrderService {

	void deleteOrder(Long orderId);

	List<Order> getInventory();

	Optional<Order> getOrderById(Long orderId);

	void placeOrder(Order body);
    
}