package io.swagger.api.service.third;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import io.swagger.configuration.ImgurConfiguration;
import io.swagger.domain.ImgurImageResponse;
import io.swagger.domain.ImgurResponse;

@FeignClient(name = "imgur", configuration = ImgurConfiguration.class, url = "${imgur.apiendpoint}")
public interface ImgurClient {

    @PostMapping(value = "3/image", consumes = {"multipart/form-data"})
    ResponseEntity<ImgurResponse<ImgurImageResponse>> uploadImage(@RequestBody Map<String, ?> formParams);
    
}