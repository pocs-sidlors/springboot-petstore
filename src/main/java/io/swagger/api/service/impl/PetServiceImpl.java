package io.swagger.api.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.api.exceptions.ApiException;
import io.swagger.api.repository.PetRepository;
import io.swagger.api.service.PetService;
import io.swagger.api.service.third.ImgurClient;
import io.swagger.domain.ImgurImageResponse;
import io.swagger.domain.ImgurResponse;
import io.swagger.model.Pet;
import io.swagger.model.Pet.StatusEnum;
import io.swagger.util.Util;

@Service
public class PetServiceImpl implements PetService {

    @Autowired
    PetRepository repository;

    @Autowired
    ImgurClient imgurClient;

    @Override
    public void addPet(Pet body) {
        repository.save(body);
    }

    @Override
    public void deletePet(Long petId) {
    	repository.deleteById(petId);
    }

    @Override
    public List<Pet> findPetsByStatus(List<String> statusList) {

       return repository.findAll().stream()
                .filter(pet-> statusList.stream()
                    .anyMatch(status -> status.equals(pet.getStatus().toString())))
                .collect(Collectors.toList());
    }

    @Override
    public List<Pet> findPetsByTags(List<String> tags) {
        return repository.findAll().stream()
                .filter(pet-> tags.stream()
                    .anyMatch(tag -> tag.equals(pet.getTags().toString())))
                .collect(Collectors.toList());

    }

    @Override
    public void uploadFile(Long petId, String additionalMetadata, MultipartFile file) throws ApiException {
        Optional<Pet> findById = repository.findById(petId);
        Map<String,Object> hashMap = new HashMap<>();
        ResponseEntity<ImgurResponse<ImgurImageResponse>> responseEntity=null;
        ImgurImageResponse data=null;
        Pet toUpdatePet=null;
        if (findById.isPresent()) {
            String encodedFile = Util.encodeFileToBase64Binary(file);
            hashMap.put("image", encodedFile);
            //rest request
            responseEntity = imgurClient.uploadImage(hashMap);
            if(responseEntity.getStatusCode().value()==200){
                data = responseEntity.getBody().getData();
                toUpdatePet=findById.get();
                toUpdatePet.addPhotoUrlsItem(data.getLink());
               repository.save(toUpdatePet);
            }else{
                throw new ApiException(500, "service image storage fail");
            }
        }
    }

    @Override
    public void updatePetWithForm(Long petId, String name, String status) {
        Optional<Pet> findById = repository.findById(petId);
        if (findById.isPresent()) {
            findById.get().setName(name);
            findById.get().setStatus(StatusEnum.fromValue(status));
            repository.save(findById.get());
        }
    }

    @Override
    public Optional<Pet> getPetById(Long petId) {
    	return repository.findById(petId);
    }

    @Override
    public void updatePet(Pet body) {
       repository.save(body);
    }
    
}