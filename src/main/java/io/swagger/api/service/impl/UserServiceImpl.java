package io.swagger.api.service.impl;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.swagger.api.repository.UserRepository;
import io.swagger.api.service.UserService;
import io.swagger.model.User;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    @Override
    public void createUser(@Valid User body) {
        repository.save(body);
    }

    @Override
    public void createUsersWithArrayInput(@Valid List<User> body) {
        body.stream().forEach(user->repository.save(user));
    }

    @Override
    public void deleteUser(String username) {
        Optional<User> findByName = repository.findByName(username);
        if (findByName.isPresent()) {
            repository.delete(findByName.get());
        }
    }

    @Override
    public Optional<User> getUserByName(String username) {
        return  repository.findByName(username);
    }

    @Override
    public void loginUser(@NotNull String username, @NotNull String password) {
        Optional<User> findByName = repository.findByName(username);
        if (findByName.isPresent()) {
            repository.loginCheck(username);
        }

    }

    @Override
    public void logoutUser() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateUser(String username, @Valid User body) {
        Optional<User> findByName = repository.findByName(username);
        if (findByName.isPresent()) {
            repository.save(body);
        }
    }
    
}