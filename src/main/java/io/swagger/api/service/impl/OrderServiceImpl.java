package io.swagger.api.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.swagger.api.repository.OrderRepository;
import io.swagger.api.service.OrderService;
import io.swagger.model.Order;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository repository;

    @Override
    public void deleteOrder(Long orderId) {
        Optional<Order> findById = repository.findById(orderId);
        if(findById.isPresent()){
         repository.delete(findById.get());
        }
    }

    @Override
    public List<Order> getInventory() {
        return repository.findAll();
    }

    @Override
    public Optional<Order> getOrderById(Long orderId) {
        return repository.findById(orderId);
    }

    @Override
    public void placeOrder(Order body) {
        repository.save(body);
    }
    
}