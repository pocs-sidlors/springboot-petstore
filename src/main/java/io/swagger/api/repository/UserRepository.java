package io.swagger.api.repository;

import java.util.Optional;
import javax.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import io.swagger.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	@Query("select u from User u where u.username =:username")
	Optional<User> findByName(String username);

	@Query("select u from User u where u.username =:username and u.userStatus=1")
	void loginCheck(@NotNull String username);


}