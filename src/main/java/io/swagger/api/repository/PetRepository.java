package io.swagger.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.swagger.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long>{

}