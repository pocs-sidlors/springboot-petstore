package io.swagger.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import feign.RequestInterceptor;

public class ImgurConfiguration {

    @Value("${imgur.clientId}")
    private String clientId;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("Authorization", "Client-ID " + clientId);
        };
    }

}
