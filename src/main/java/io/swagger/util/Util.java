package io.swagger.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class Util {
    private Util(){}

    public static String encodeFileToBase64Binary(MultipartFile file) {
        String encodedfile = null;
        try {
            InputStream fileInputStreamReader = file.getInputStream();
            byte[] bytes = file.getBytes();
            fileInputStreamReader.read(bytes);
            encodedfile = new String(Base64.encodeBase64(bytes), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedfile;
    }

	public static String getFileContentBase64(String fileName) throws IOException{
		File file =new File("src/test/resources/"+fileName);
        return FileUtils.readFileToString(file, "utf-8");
	}


}