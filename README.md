[![pipeline status](https://gitlab.com/pocs-sidlors/springboot-petstore/badges/develop/pipeline.svg)](https://gitlab.com/pocs-sidlors/springboot-petstore/-/commits/develop)

# swagger-pet-store-spring-boot
Generating spring boot micro-service from swagger editor

### 2 simple changes to pom file

1. You can check mvnrepository.com for the latest spring boot and spring fox version
    -   Spring boot version - 2.2.5.RELEASE
    -   Spring fox version -  2.9.2
    -   Spring cloud version - Hoxton.SR3
2. Swagger yaml file attached in spring boot resources folder
3. Swagger ui -  http://localhost:8080/v2/swagger-ui.html

#### This method can also be followed for the TMForum swagger code gen - OpenAPI's
https://projects.tmforum.org/wiki/display/API/Open+API+Table?_ga=2.135373297.1527475273.1518982686-1034505948.1514628225
 
If this link doesn't work
1. https://www.tmforum.org/
2. frameworx - OpenAPI
3. Developer Portal
4. Accompanying docs and swagger tools can be found in the table
5. User account for TMForum is required

## JWT and Security

### Spring Boot Rest Authentication with JWT Token Flow

![](https://www.techgeeknext.com/jwt/spring-boot-jwt-workflow.JPG)

### Get a valid token 

For operations that need Authorization you need to do a POST to localhost:8080/v2/authenticate

* username: techgeeknext
* password: password

![](https://i.imgur.com/BfZSbt8.png)

so.. add Authorization header in delete pet service method DELETE

![](https://i.imgur.com/WHJRKSU.png)

___

## Manejo de Excepciones 

There are many ways to handle exceptions for an api in spring boot [baeldung](https://www.baeldung.com/exception-handling-for-rest-with-spring)

At the controller level using the @ExceptionHandler annotation in a method of the controller class

```java
public class FooController{
    
    //...
    @ExceptionHandler({ CustomException1.class, CustomException2.class })
    public void handleException() {
        //
    }
}
```

Another approach is a class that handles all the exceptions of your endpoints

### Anotacion @ControllerAdvice

The @ControllerAdvice annotation was first introduced in Spring 3.2. It allows you to handle exceptions throughout the application, not just an individual controller.
You can think of it as a trap for exceptions thrown by methods annotated with @RequestMapping.
Handlers are declared in this class, one per exception,
as each exception needs to be handled differently so that the API user will get an understandable error message.

In this application, ContentNotAllowedException contains a list of inappropriate words, while UserNotFoundException only contains a message.
Let's write a controller adviser class to handle those exceptions.

The @ControllerAdvice annotation allows us to consolidate our multiple sparse @ExceptionHandlers from before into a single global error handling component. It gives us full control over the body of the response and also over the status code.

Provides a mapping of several exceptions to the same method, to be handled together.
One thing to note here is to match the exceptions declared with @ExceptionHandler with the exception used as an argument to the method.

If these don't match, the compiler won't complain, for no reason, and Spring won't complain either,
however when the exception is thrown at runtime, the exception resolution mechanism will fail with:
java.lang.IllegalStateException: There is no proper solver for argument [0] [type = ...]
HandlerMethod details: ...

GlobalExceptionHandler was annotated with @ControllerAdvice, so it will trap controller exceptions on all endpoints.

 
 ```java
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler(value = { ApiException.class })
    protected ResponseEntity<Object> handleApieException(Exception ex, WebRequest request) {
        ModelApiResponse error = new ModelApiResponse(500,"ERROR" ,ex.getMessage());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, error,  new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = {  NotFoundException.class })
    protected ResponseEntity<Object> handleNotFoundException(Exception ex, WebRequest request) {
        ModelApiResponse error = new ModelApiResponse(403,"NOT FOUND" ,ex.getMessage());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, error,  new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}}
}
```








